#include<iostream>
#include<fstream>
#include<string>
using namespace std;
void solve_sudoku(int S[9][9], int x, int y);// ham giai
int feasible(int S[9][9], int x, int y, int k);// ham kiem tra gia tri k co tm hay khong
void printSolution(int S[9][9]);// ham in ket qua ra man hinh
int check_input(int S[9][9]);// ham kiem tra de
int main()
{
	int A[9][9];// dung de luu sudoku
	int flag;
	fstream f;
	f.open("baitoan.txt");// ten file chua sudoku can giai
	//dua sudoku tu file *.txt vao A[9][9]
	for(int i=0;i<9;i++)
	  for(int j=0;j<9;j++){
	  	f>>A[i][j];
	  }
	f.close();
	if (check_input(A) == 1){
	solve_sudoku(A,0,0);
	}
	return 0;
}
void solve_sudoku(int S[9][9], int x, int y)
{
	if(y == 9) // het 1 hang
	{
		if(x == 8)// dien het den A[8][8]
		{
			printSolution(S);
			exit(0);
		} 
		else  solve_sudoku(S, x+1,0);
	} 
	else if(S[x][y] == 0)
	{
		int k = 0;
		for (k = 1; k <=9; k++)
		{
			if(feasible(S,x,y,k))//loc tap UCV
			{
				S[x][y] = k;
				solve_sudoku(S, x, y+1);
				S[x][y] = 0;//khoi phuc trang thai de quay lui
			}
		}
	} else solve_sudoku(S,x,y+1);//khi o da duoc dien
	
}


int feasible(int S[9][9], int x, int y, int k)
{
	int i = 0, j = 0;
	//kiem tra theo hang
	for(i = 0; i <9 ; i++){
		if(S[x][i] == k) return 0;
	}
	//kiem tra theo cot
	for(i = 0; i <9 ; i++){
			if(S[i][y] == k) return 0;
		}
	// kiem tra bang 3x3	
	int a = x/3, b = y/3;
	for(i = 3*a; i < 3*a+3; i++){
		for(j = 3*b; j < 3*b+3; j++){
			if(S[i][j] == k) return 0;
		}
	}
	return 1;

}
int check_input(int S[9][9]){
	int a; 
	int b;
	int tem;
	int flag = 1;// danh dau khi nao sai
	for( a = 0; a < 9; a ++)
	   for(b = 0; b < 9; b ++){
	   	if(S[a][b] != 0) {
	   	// gia su nhu vi tri do chua duoc dien	
	   	tem = S[a][b];
	   	S[a][b] = 0;
		flag = feasible(S, a, b, tem);
		S[a][b] = tem;
		if(flag == 0){
		cout << " Sai de !!! " ;
		return 0;
		}
	    }
	   }
	return 1;   
}
void printSolution(int S[9][9])
{   
	ofstream file ("ketqua.html");
	file <<"<BODY LEFTMARGIN=560 TOPMARGIN=70 RIGHTMARGIN=300 BOTTOMMARGIN=250>";// chinh ra giua
	file << "<h2>KET QUA SUDOKU</h2>";
	file << "<style> table, th { border: 1px solid black; border-collapse: collapse;} th { padding: 9px; background-color: #f1f1c1;}</style>" ;
	file << "<table border= 6 >";
	int i = 0, j = 0;
	for(i = 0; i <  9; i++)
	{
		file<<"<tr>";
		for(j = 0; j <  9; j++)
		{
			file<<"<th>"<<" "<<S[i][j]<<"</th>";
		}
		file<<"</tr>";
	}
	file<<"</table>";
	file.close();
	char input[50]="C:/Users/Admin/Desktop/sudoku/ketqua.html";
	system(input);
}


