explain how the code run:
* input:- mang so nguyen 9x9, duoc luu duoi dang 1 file text
- bai toan duoc luu nhu mot mang hay mot day so trong file txt
- cac vi tri so khong duoc nhap vao se duoc coi bang 0
* chuc nang cua cac ham:
- check_input: kiem tra du lieu dau vao, cac so co trong mang 9x9 khong duoc lap lai trong hang, cot, va trong pham vi
3x3 ung voi vi tri cua no, neu thoa man ham tra ve gia tri 1, neu khong thoa man, ham tra ve gia tri 0.
- feasible: duyet tat ca cac so trong hang, cot va trong pham vi 3x3, tra ve gia tri 1 neu so do chua xuat hien,
gia tri 0 neu so do da xuat hien.
- printSolution: in ra ket qua duoi dang 1 file html, mot bang so 9x9 thoa man tro choi sudoku.
- solve_sudoku: ham giai sudoku, su dung backtracking, ham nay yeu cau du lieu dau vao gom co 1 mang so 9x9, vi tri cot x, hang y cua 
phan tu dang xet.
* chuong trinh hoat dong nhu sau:
- dau tien, chuong trinh se khai bao mot mang hai chieu A[9][9], sau do mo file txt co chua noi dung bai toan, gan tung 
phan tu vao cac vi tri tuong ung trong mang A.
- chuong trinh goi ham kiem tra du lieu dau vao, check_input tra ve 1 neu du lieu dau vao thoa man dieu kien, tra ve 0 neu 
khong thoa man.
- neu du lieu dau vao thoa man, chuong trinh se tien hanh giai sudoku bang cach goi ham solve_sudoku:
  + khi ham solve_sudoku duoc goi lan dau, solve_sudoku(A[9][9], 0, 0), no se bat dau kiem tra phan tu tai vi tri A[0][0], neu phan tu nay bang 0,
  mot vong lap for chay tu 1 den 9 duoc su dung de duyet so ung voi vi tri nay, moi lan vong lap for thuc hien, no se goi
  ham feasible kiem tra xem so do co thoa man hay khong, neu thoa man thi tai vi tri se duoc gan gia tri bang so do, 
  sau do ham solve_sudoku tiep tuc duoc goi va lap lai qua trinh nhu tren, tham so y (cot) luc nay se duoc tang len 1, chuong trinh thuc hien kiem tra de
  dien so vao vi tri tiep theo cua hang.
 + khi gia tri cua o da duoc dien, ham se nhay toi o tiep theo cua hang va thuc hien qua trinh nhu tren.
 + tro choi duoc giai khi gia tri cua y duoc tang den 9, va gia tri cua x duoc tang den 8, tuc la da dien xong den A[8][8]. Luc nay chuong trinh  goi 
 ham printSolution in ra loi giai cua bai toan duoi dang 1 file html.
  
     
                
                
                
       
