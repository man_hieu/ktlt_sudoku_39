#include<iostream>
#include<fstream>
using namespace std;
void solve_sudoku(int S[9][9], int x, int y);
int feasible(int S[9][9], int x, int y, int k);
void printSolution(int S[9][9]);
int main()
{
	int A[9][9];
	fstream f;
	f.open("vidu.txt");
	for(int i=0;i<9;i++)
	  for(int j=0;j<9;j++){
	  	f>>A[i][j];
	  }
	f.close();
    for(int i=0;i<9;i++)
	  for(int j=0;j<9;j++)
	  {
	  	cout<<"  "<<A[i][j];
	  	if(j%9==8) cout<<endl;
	  }
	solve_sudoku(A,0,0);
	return 0;
}
void solve_sudoku(int S[9][9], int x, int y)
{
	if(y == 9)
	{
		if(x == 8)
		{
			printSolution(S);
			exit(0);
		} else {
			solve_sudoku(S, x+1,0);
		}
	} else if(S[x][y] == 0)
	{
		int k = 0;
		for (k = 1; k <=9; k++)
		{
			if(feasible(S,x,y,k))
			{
				S[x][y] = k;
				solve_sudoku(S, x, y+1);
				S[x][y] = 0;
			}
		}
	} else {
		solve_sudoku(S,x,y+1);
	}
}


int feasible(int S[9][9], int x, int y, int k)
{
	int i = 0, j = 0;
	for(i = 0; i <9 ; i++){
		if(S[x][i] == k) return 0;
	}
	for(i = 0; i <9 ; i++){
			if(S[i][y] == k) return 0;
		}
	int a = x/3, b = y/3;
	for(i = 3*a; i < 3*a+3; i++){
		for(j = 3*b; j < 3*b+3; j++){
			if(S[i][j] == k) return 0;
		}
	}
	return 1;

}
void printSolution(int S[9][9])
{   
	ofstream file ("ketqua.txt");
	file << "ket qua sudoku \n";
	int i = 0, j = 0;
	for(i = 0; i <  9; i++)
	{
		file<<endl;
		for(j = 0; j <  9; j++)
		{
			file<<"|"<<S[i][j];
		}
		file <<"|";
		file << endl;
		if(i<8)
		file <<"+-+-+-+-+-+-+-+-+-+";
	}
	file.close();
}

